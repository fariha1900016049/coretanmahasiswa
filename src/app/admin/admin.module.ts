import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Routes, RouterModule} from '@angular/router';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { GalleryComponent } from './gallery/gallery.component';
import { ImagesComponent } from './images/images.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ProductComponent } from './product/product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path:"",
    component:AdminComponent,
    children:[
      {
        path:"dashboard",
        component:DashboardComponent
      },
      {
        path:'product',
        component:ProductComponent
      }, 
      {
          path:'gallery',
          component:GalleryComponent
      },
      {
        path:"",
        pathMatch:"full",
        redirectTo: "/admin/dashboard"
      }
    ]
  },
]

@NgModule({
  declarations: [AdminComponent, DashboardComponent, ImagesComponent, GalleryComponent, ImagesComponent, ProductComponent, ProductDetailComponent],
  entryComponents: [
    ImagesComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatFormFieldModule,
    MatListModule,
    MatSidenavModule,
    MatDialogModule,
    ImageCropperModule,
    FormsModule
  ]
})
export class AdminModule { }

